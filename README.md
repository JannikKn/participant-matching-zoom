# Participant Matching Zoom

This is a short Python Script to match Zoom Participants to a list of students that participate in a Zoom Meeting
In the current state this is only a prototype which can be enhanced when it works well after some tests

At the moment some refactoring is still needed for code readability. But the code works and can be tested using participantNames (file containing a list of participants that should be in the zoom meeting) and readNames (file containing names of students that were actually in the zoom meeting)
The readNames file was generated using a Screenshot and pytesseract because this is currently the only way for me to obtain test data.
