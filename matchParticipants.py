import Levenshtein as Levenshtein


# class to store information and improve code readability
class Participant:
    def __init__(self, firstName, lastname):
        self.firstName = firstName
        self.lastname = lastname


def fuzzyContains(list, element, maxDistance=2):
    # use levensthein distance for a fuzzy compare
    min = 5
    for elem in list:
        distance = Levenshtein.distance(elem, element)
        if distance < min:
            min = distance
    if min <= maxDistance:
        return True
    else:
        return False


def matchParticipants(participantsZoom, participantsList, distance=0):
    foundParticipants = []
    # check if lastname can be found in participants
    for participant in participantsList:
        for zoomParticipant in participantsZoom:    # a levenshtein distance of 2 equals one typo (delete wrong char + edit correct char)
            if fuzzyContains(zoomParticipant, participant.lastname, 2) and \
                    fuzzyContains(zoomParticipant, participant.firstName, distance):
                foundParticipants.append(participant)
                participantsZoom.remove(zoomParticipant)
                break

    participants = [x for x in participantsList if x not in foundParticipants]
    return foundParticipants, participants, participantsZoom


def iterativeParticipantMatch(participantsZoom, participantsList, minDistance, maxDistance, printIterationOutput = False):
    curr_dist = minDistance + 1
    participants = []
    for participantName in participantsList:
        words = participantName.split(" ")
        participants.append(Participant(words[1], words[0]))
    zoomParticipants = [[]]
    for name in participantsZoom:
        words = name.split(" ")
        zoomParticipants.append(words)

    curr_matchedParticipants = []
    curr_notFoundParticipants = []
    curr_notMatchedParticipants = []
    matchedParticipants, notFoundParticipants, notMatchedParticipants = matchParticipants(zoomParticipants, participants)
    if printIterationOutput:
        print("Matched at Intitial check: ")
        printCompleteName(matchedParticipants)
        print("Not found from participant list at Initial check: ")
        printCompleteName(notFoundParticipants)
        print("Not Matched Participants from zoom Meeting participants at Initial check: ")
        print(notMatchedParticipants)
        print("---------------------------------------------------------------------------------------------------")
    participants = [part for part in participants if part not in matchedParticipants]
    while curr_dist <= maxDistance:
        curr_matchedParticipants, curr_notFoundParticipants, curr_notMatchedParticipants = matchParticipants(notMatchedParticipants, participants, curr_dist)
        matchedParticipants = matchedParticipants + curr_matchedParticipants
        notMatchedParticipants = [part for part in notMatchedParticipants if part not in curr_matchedParticipants]
        participants = [part for part in participants if part not in curr_matchedParticipants]
        if printIterationOutput:
            print("Matched at Iteration: " + str(curr_dist - minDistance))
            printCompleteName(curr_matchedParticipants)
            print("Not found from participant list at Iteration: " + str(curr_dist - minDistance))
            printCompleteName(curr_notFoundParticipants)
            print("Not Matched Participants from zoom Meeting participants at Iteration: " + str(curr_dist - minDistance))
            print(curr_notMatchedParticipants)
            print("---------------------------------------------------------------------------------------------------")
        curr_dist += 1
    return matchedParticipants, curr_notFoundParticipants, curr_notMatchedParticipants

def printCompleteName(participantsList):
    for p in participantsList:
        print("\t" + p.lastname + ", " + p.firstName)


# MAIN PROGRAM (Still needs some refactoring)
participants_present = []
with(open("participantNames", "r")) as file:
    for name in file.read().splitlines():
        participants_present.append(name)


participants = []
with(open("readNames", "r")) as file:
    for name in file.read().splitlines():
        participants.append(name)

found, notFound, notMatched = iterativeParticipantMatch(participants, participants_present, 0, 1, True)
print("Students that did participate: ")
printCompleteName(found)
print("Students that should participate but could not be found")
printCompleteName(notFound)
print("Students that were in the zoom meeting but could not be matched to the participant list")
print(notMatched)




