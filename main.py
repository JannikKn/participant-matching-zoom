import pytesseract
from PIL import ImageGrab
from win32api import GetSystemMetrics
from datetime import datetime

# TEST SKRIPT TO TRY OUT FUNCTIONING PYTESSERACT
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
# image = cv.imread('PIC_Labor1_Gruppe4.PNG')
# text = pytesseract.image_to_string(image, lang='spa')
# print('Texto: ', text)

# cv.imshow('Image', image)
# cv.waitKey(0)
# cv.destroyAllWindows()

# get scrren width and height
width = GetSystemMetrics(0)
height = GetSystemMetrics(1)

def imToString():
    # Path of tesseract executable (CAUTION: this needs to be changed when using this script on a different PC)
    pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
    # take screenshot from right screen place to get participants list without too much distracting text
    cap = ImageGrab.grab(bbox=(0.75 * width, 0, width, height))
    return pytesseract.image_to_string(cap)


imageString = imToString()
lines = imageString.splitlines()

names = set()
startNames = False
# filter participants from image text
for line in lines:
    if line.__contains__("Einladen"):
        break
    if startNames:
        names.add(line)
    if line.__contains__("Teilnehmer"):
        startNames = True

# remove empty lines that get read between names
names.remove("")

# get date and time
now = datetime.now()

with(open("output.txt", "w")) as file:
    file.write(now.__str__() + "\n\n")
    for name in names:
        file.write(name + "\n")
